// © 2020 Raptor Engineering, LLC
//
// Released under the terms of the GPL v3
// See the LICENSE file for full details

#ifndef _FSI_H
#define _FSI_H

#include <stdint.h>

typedef enum
{
    FSI_DATA_LENGTH_BYTE = 0,
    FSI_DATA_LENGTH_HALFWORD = 1,
    FSI_DATA_LENGTH_WORD = 2
} fsi_data_length_t;

// Peripheral registers
#define FSI_MASTER_REG_DEVICE_ID_HIGH 0x0
#define FSI_MASTER_REG_DEVICE_ID_LOW  0x4
#define FSI_MASTER_REG_DEVICE_VERSION 0x8
#define FSI_MASTER_REG_SID_ADR        0xc
#define FSI_MASTER_REG_CONTROL        0x10
#define FSI_MASTER_REG_STATUS         0x14
#define FSI_MASTER_REG_TX_DATA        0x18
#define FSI_MASTER_REG_RX_DATA        0x1c
#define FSI_MASTER_REG_DMA_IRQ        0x20

#define FSI_MASTER_SID_SLAVE_ID_MASK     0x3
#define FSI_MASTER_SID_SLAVE_ID_SHIFT    29
#define FSI_MASTER_SID_ADDRESS_MASK      0x1fffff
#define FSI_MASTER_SID_ADDRESS_SHIFT     8
#define FSI_MASTER_SID_DATA_LENGTH_MASK  0x3
#define FSI_MASTER_SID_DATA_LENGTH_SHIFT 0

#define FSI_MASTER_CTL_CMD_ISSUE_DELAY_MASK  0xff
#define FSI_MASTER_CTL_CMD_ISSUE_DELAY_SHIFT 8
#define FSI_MASTER_CTL_DATA_DIRECTION_MASK   0x1
#define FSI_MASTER_CTL_DATA_DIRECTION_SHIFT  1
#define FSI_MASTER_CTL_ENABLE_CRC_MASK       0x1
#define FSI_MASTER_CTL_ENABLE_CRC_SHIFT      17
#define FSI_MASTER_CTL_ENABLE_EER_MASK       0x1
#define FSI_MASTER_CTL_ENABLE_EER_SHIFT      16
#define FSI_MASTER_CTL_CYCLE_START_MASK      0x1
#define FSI_MASTER_CTL_CYCLE_START_SHIFT     0

#define FSI_MASTER_STAT_CYCLE_ERROR_MASK     0x7
#define FSI_MASTER_STAT_CYCLE_ERROR_SHIFT    8
#define FSI_MASTER_STAT_CYCLE_COMPLETE_MASK  0x1
#define FSI_MASTER_STAT_CYCLE_COMPLETE_SHIFT 0

#define FSI_DIRECTION_READ  0
#define FSI_DIRECTION_WRITE 1

static inline uint32_t read_openfsi_register(unsigned long base_address, uint8_t reg)
{
    return *((volatile uint32_t *)(base_address + reg));
}

static inline void write_openfsi_register(unsigned long base_address, uint8_t reg, uint32_t data)
{
    *((volatile uint32_t *)(base_address + reg)) = data;
}

int run_pre_ipl_fixups(void);
int start_ipl(int side);
int get_sbe_status(void);

#endif // _FSI_H
